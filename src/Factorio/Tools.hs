module Factorio.Tools where

import Prelude
import Data.Monoid
import Data.Foldable (foldl')

-- ============================================================================
-- Reactor Tools
-- ============================================================================
newtype ReactorOutput = ReactorOutput Int

instance Show ReactorOutput where
  show (ReactorOutput n) = show n <> " MW"


data ReactorGridShape
  = Square Int
  | Rectangle Int Int
  deriving Show

reactorOutput :: ReactorGridShape -> ReactorOutput
reactorOutput (Square n)
  | n <= 0 = ReactorOutput 0
  | n == 1 = ReactorOutput 40
  | n == 2 = ReactorOutput (4 * 3 * 40)
  | otherwise =
    let inner = ((n - 2) * (n - 2)) * 5 * 40
        corners = 4 * 3 * 40
        borders = (n - 2) * 4 * 4 * 40
    in ReactorOutput (inner + corners + borders)

reactorOutput (Rectangle w h)
  | w <= 0 || h <= 0 = ReactorOutput 0
  | w == h = reactorOutput (Square w)
  | w == 1 && h == 2 = undefined
  | w == 2 && h == 1 = undefined
  | w == 1 && h > 2 = undefined
  | w > 2 && h == 1 = undefined
  | otherwise = undefined

-- ============================================================================
-- Crafting Tools
-- ============================================================================
newtype Inputs = Inputs [(Material, Count)] deriving (Show, Eq)
newtype Outputs = Outputs [(Material, Count)] deriving (Show, Eq)
newtype CraftTime = CraftTime Int deriving (Show, Eq)
newtype Count = Count Int deriving (Show, Eq)

data Recipe
  = Recipe CraftTime Inputs Outputs Maker
  deriving (Show, Eq)

renderCraftExpr :: CraftExpr -> String
renderCraftExpr = go 0
    where go :: Int -> CraftExpr -> String
          go indent ex = case ex of
            NoSolution -> "no solution"
            CraftStep m c mk t xs ->
                show m
                <> " need:" <> renderCount c
                <> " in:" <> show mk
                <> " time:" <> renderCraftTime t
                <> "\n"
                <> foldl' (\acc x -> acc <> replicate (indent+2) ' ' <> go (indent+2) x) "" xs

          renderCount :: Count -> String
          renderCount (Count n) = show n

          renderCraftTime (CraftTime n) = show n

-------------------------------------------------------------------------------
-- Crafting Core
-------------------------------------------------------------------------------
data CraftExpr
  = CraftStep Material Count Maker CraftTime [CraftExpr]
  | NoSolution
  deriving (Show, Eq)

-- |
-- the solver algorithm
--
-- TODO: returns only the first match found if alternatives exist for
-- crafting a particular step component. extend this to support
-- generating whole crafting trees, or to use a more guided approach
-- in constructing a crafting sequence. See: 'findInputs'
solve :: Material -> Count -> [Recipe] -> CraftExpr
solve mat _howMany recipes' =
    let needs = findInputs mat recipes'
    in case needs of
         RecipeNotFound -> NoSolution
         RecipeFound m yield t is ->
             CraftStep mat yield m t (foldl' step [] ((\(Inputs x) -> x) is))

    where step :: [CraftExpr] -> (Material, Count) -> [CraftExpr]
          step acc (m,c) = solve m c recipes : acc

findInputs :: Material -> [Recipe] -> RecipeLookup
findInputs mat recipes' =
    case filterByMat mat recipes' of
      [] -> RecipeNotFound
      -- use only first recipe found
      (r@(Recipe t is _ maker) : _) ->
          RecipeFound maker (recipeYield mat r) t is
    where recipeYield m (Recipe _ _ (Outputs os) _) =
              foldl' (\acc (m',c) -> if m == m' then c else acc) (Count 0) os


filterByMat :: Material -> [Recipe] -> [Recipe]
filterByMat mat =
    filter (yieldsMaterial mat)
    where yieldsMaterial m (Recipe _ _ (Outputs os) _) = m `elem` fmap fst os

-------------------------------------------------------------------------------
-- Crafting Related Types
-------------------------------------------------------------------------------
data RecipeLookup
  = RecipeNotFound
  | RecipeFound Maker Count CraftTime Inputs

data Material
  = Ore OreType
  | Natural NatureType
  | Crushed OreTypePlus
  | Plate OreTypePlus
  | Crystal OreType
  | PurifiedCrystal OreType
  | Science ScienceType
  | Fluid FluidType
  | Fertilizer FertilizerType
  | Component ComponentType
  deriving (Show, Eq)

data ComponentType
  = IronGearWheel
  deriving (Show, Eq)


data FertilizerType
  = Ash
  | BasicFertilizer
  | AdvancedFertilizer
  deriving (Show, Eq)

data NatureType
  = RawWood
  | WoodPulp
  | Charcoal
  | Seed
  | Seedling
  deriving (Show, Eq)

data ScienceType
  = Red
  | Green
  deriving (Show, Eq)

data FluidType
  = Water
  deriving (Show, Eq)

data OreTypePlus
  = BaseOre OreType
  | Ferrous
  | Cupric
  | Iron
  | Copper
  | Aluminum
  | Chrome
  | Cobalt
  | Gold
  | Manganese
  | Nickel
  | Platinum
  | Silver
  | Tin
  | Titanium
  | Tungsten
  | Zinc
  | Steel
  deriving (Show, Eq)

data Maker
  = Furnace
  | ChemicalPlant
  | Miner
  | Assembler
  | Crusher
  | Sorter
  deriving (Show, Eq)


data OreType
  = Saphirite
  | Coal
  | Stone
  | Stiratite
  | Jivolite
  | Crotinnium
  | Bobmonium
  | Rubyte
  deriving (Show, Eq)

-------------------------------------------------------------------------------
-- Recipe Dictionary
-------------------------------------------------------------------------------

-- this is based on an Angel/Bob's modded run. Ideally, this recipe
-- dictionary would be generated by parsing game data files (that'd
-- require parsing Lua), or, scraping some other resource.
recipes :: [Recipe]
recipes = [
      Recipe (CraftTime 7)
             (Inputs [(Crushed (BaseOre Stiratite), Count 3)])
             (Outputs [(Plate Copper, Count 2)])
             Furnace

    , Recipe (CraftTime 7)
             (Inputs [(Crushed (BaseOre Saphirite), Count 3)])
             (Outputs [(Plate Iron, Count 2)])
             Furnace

    , Recipe (CraftTime 1)
             (Inputs [(Plate Iron, Count 4)])
             (Outputs [(Component IronGearWheel, Count 2)])
             Assembler

    , Recipe (CraftTime 5)
             (Inputs [
                     (Component IronGearWheel, Count 1)
                   , (Plate Copper, Count 1)
                  ])
             (Outputs [(Science Red, Count 1)])
             Assembler

    , Recipe (CraftTime 1)
             (Inputs [])
             (Outputs [(Ore Saphirite, Count 1)])
             Miner

    , Recipe (CraftTime 1)
             (Inputs [])
             (Outputs [(Ore Stiratite, Count 1)])
             Miner

    , Recipe (CraftTime 1)
             (Inputs [(Ore Stiratite, Count 2)])
             (Outputs [
                     (Crushed (BaseOre Stiratite), Count 2)
                   , (Crushed (BaseOre Stone), Count 1)
                   ])
             Crusher

    , Recipe (CraftTime 1)
             (Inputs [(Ore Saphirite, Count 2)])
             (Outputs [
                     (Crushed (BaseOre Saphirite), Count 2)
                   , (Crushed (BaseOre Stone), Count 1)
                   ])
             Crusher

    ]

-------------------------------------------------------------------------------
-- Sugar
-------------------------------------------------------------------------------
furnace :: Maker
furnace = Furnace

chemicalPlant :: Maker
chemicalPlant = ChemicalPlant

miner :: Maker
miner = Miner

assembler :: Maker
assembler = Assembler

crusher :: Maker
crusher = Crusher

sorter :: Maker
sorter = Sorter

saphirite :: OreType
saphirite = Saphirite

baseSaphirite :: OreTypePlus
baseSaphirite = BaseOre saphirite


recipe :: Int -> Inputs -> Outputs -> Maker -> Recipe
recipe t = Recipe (CraftTime t)

inputs :: [(Material, Count)] -> Inputs
inputs = Inputs

outputs :: [(Material, Count)] -> Outputs
outputs = Outputs

crushedStiratite :: Material
crushedStiratite = Crushed (BaseOre Stiratite)

copperPlate :: Material
copperPlate = Plate Copper

item :: Material -> Int -> (Material, Count)
item m n = (m, Count n)
