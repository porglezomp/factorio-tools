module Main where

import System.Environment
import Test.DocTest

main :: IO ()
main = do
  args <- getArgs
  doctest (["src"] ++ args)
